class Singleton private constructor(){
    var sampleName:String?=""
    init {
        "This is {$this} singleton".also(::println)
    }
    private object Holder{
        val INSTANCE= Singleton()
    }
    companion object {
        val instance:Singleton by lazy { Holder.INSTANCE }
    }

}